# Test de integracion en polymer
## 1. crear una aplicacion

nos movemos a nuestro directorio de proyectos 
-
~~~ 
  cd proyectos
~~~ 

creamos un directorio para nuestra app polymer 
-
~~~ 
  mkdir shop
~~~ 

nos movemos al directorio de la app 
-
~~~ 
  cd shop
~~~ 

Es neceario tener el polymer cli instalado, para mas detalles 
-
~~~ 
  https://www.polymer-project.org/2.0/docs/tools/polymer-cli
~~~ 

creamos una app polymer 
-
~~~ 
  polymer init
~~~ 

Elegimos "shop" que es la aplicacion compleja de polymer
-
~~~  
  ? Which starter template would you like to use? 
    polymer-2-element - A simple Polymer 2.0 element template 
    polymer-2-application - A simple Polymer 2.0 application 
    polymer-2-starter-kit - A Polymer 2.x starter application template, with navig
  ation and "PRPL pattern" loading 
  ❯ shop - The "Shop" Progressive Web App demo 
~~~ 

Instalamos las dependencias del proyecto 
-
~~~ 
  polymer install
~~~ 

comprobamos que todo funciona
-
~~~ 
   polymer serve
~~~ 

Deberias ver algo en la consola como 
-
~~~ 
info:    Files in this directory are available under the following URLs
      applications: http://127.0.0.1:8081
      reusable components: http://127.0.0.1:8081/components/shop/
~~~ 


si abres la url en el navegador veras la app 
* (el puerto puede cambiar si lo tenias ocupado)
-
~~~ 
http://127.0.0.1:8081
~~~ 


## 2. subir la app a bitbucket

#### crear el repositorio

Puedes ver un tutorial de estas cosas en 
-
~~~ 
https://confluence.atlassian.com/bitbucket/create-a-git-repository-759857290.html
~~~ 

En el momento que tienes una app funcionando es el momento de poner la en un repositorio de codigo 
-
~~~ 
*ir a bitbucket 

*repository / create a repository 

*darle nombre cells-ngob-simple-demo

*desmarcar privado si aplica 

*pulsar boton crear 
~~~ 

#### inicializar el repo en local, vincularlo con bitbucket y subirlo

Puedes ver un tutorial de estas cosas en 
-
~~~ 
http://rogerdudler.github.io/git-guide/index.es.html
~~~

*indicar que quieres un proyecto git 
-
~~~ 
git init
~~~

*en la pagina de despues de crear el repo copiar la ruta para incluir al repo (algo como esto )*
-
~~~ 
 git remote add origin https://guzmanator@bitbucket.org/guzmanator/shop.git
~~~

comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~

incluir los ficheros al git 
-
~~~ 
       git add .
~~~

hace run comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "Initial commit"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
* (el numero del tag debe de coincidir con la version del bower)
-
~~~ 
git tag -a 2.0.0 -m "init version 2.0.0" 
~~~

subirla al repo 
-
~~~ 
git push origin 2.0.0
~~~


## 3. subir la app a firebase

Puedes ver la documentacion completa de la instalacion en
-
~~~ 
   https://firebase.google.com/docs/hosting/deploying?authuser=2&hl=es-419
~~~

#### crear el proyecto en firebase

vamos a la consola
-
~~~ 
    https://console.firebase.google.com/u/2/?hl=es-419&pli=1
~~~

Pulsamos en crear nuevo proyecto 
-
~~~ 
  agregar proyecto 
~~~


elige un nombre 
-
~~~ 
Nombre del proyecto
  shop
~~~

Elige el pais donde estaran tus usuarios
-
~~~ 
País/Región
  México
~~~

Pulsa el boton de crear
-
~~~ 
  CREAR PROYECTO
~~~

#### instalar las firebase tools y crear el directorio de despliegue

ejecuta el comando de instalacion si no lo has hecho ya 
-
~~~ 
npm install -g firebase-tools
~~~

ve a un directorio diferente de tu app
-
~~~ 
  cd ..
  mkdir firebaseDeployShop
  cd firebaseDeployShop
~~~

*¿porque?* a mi me gusta no mezclar la configuracion de deploy con la del proyecto, cada cosa en su sitio, si mañana quiero subir a heroku puedo hacerlo sin buscar que es de fierebase y que de mi app


hacer login desde la consola
-
~~~ 
  firebase login
~~~

te preguntara si quieres compartir datos anonimos del cli 
-
~~~ 
? Allow Firebase to collect anonymous CLI usage information? Yes
~~~

te ofrecera una url para que te loguees 
-
~~~ 
Visit this URL on any device to log in:
https://accounts.google.com/o/oauth2/auth?client_id=563..................................................................................................................................................................

Waiting for authentication...
~~~ 

*(url real ocultada para evitar hackers)

copia y pega la url en un navegador, elige tu correo, acepta el acceso y cuando se cierre el navegador veras en la consola algo como 
-
~~~ 
✔  Success! Logged in as guzmanpaniagua@gmail.com
~~~


Crear la estructura de despliegue de firebase 
-
~~~ 
  firebase init
~~~


te pregunta que features, yo he elegido solo hosting
-
~~~ 
? Which Firebase CLI features do you want to setup for this folder? Press Space to select features, then Enter to confirm your choices. 
 ◯ Database: Deploy Firebase Realtime Database Rules
 ◯ Functions: Configure and deploy Cloud Functions
❯◉ Hosting: Configure and deploy Firebase Hosting sites
~~~

te pregunta en que proyecto desplegaras, yo he  elegido el de shop 
-
~~~ 
=== Project Setup

First, let's associate this project directory with a Firebase project.
You can create multiple project aliases by running firebase use --add, 
but for now we'll just set up a default project.

? Select a default Firebase project for this directory: 
  firebase-polymer (fir-polymer-75894) 
  final-proyect (final-proyect-2f2c6) 
  final-forum (final-forum-b4661) 
❯ shop (shop-e7df4) 
  pokemonIdeas (pokemonideas-6b58e) 
  [create a new project] 
  [don't setup a default project] 
~~~

te pregunta el directorio donde pones el codigo a subir, yo he elegido public que es el por defecto 
-
~~~ 
=== Hosting Setup

Your public directory is the folder (relative to your project directory) that
will contain Hosting assets to be uploaded with firebase deploy. If you
have a build process for your assets, use your build's output directory.

? What do you want to use as your public directory? (public) 
~~~

te pregunta que si queres se una spa, yo he elegido N
-
~~~
? Configure as a single-page app (rewrite all urls to /index.html)? (y/N) N
~~~

* ¿porque N? por si quiero que accedan a otra url diferente del index 

deberias de poder ver una estructura como 
-
~~~
ls -la
total 20
drwxrwxr-x 3 guzman guzman 4096 ago 24 12:42 .
drwxrwxr-x 9 guzman guzman 4096 ago 24 12:30 ..
-rw-rw-r-- 1 guzman guzman  134 ago 24 12:42 firebase.json
-rw-rw-r-- 1 guzman guzman   52 ago 24 12:42 .firebaserc
drwxrwxr-x 2 guzman guzman 4096 ago 24 12:42 public
~~~

#### cosntruir y copiar la app al directorio de despliegue

volvemos al directorio de nuestra app 
-
~~~ 
  cd ..
  cd shop
~~~

Lanzamos el comando de construir app 
-
~~~ 
 polymer build
~~~

vamos al directorio donde genera la app lista para subir 

-
~~~ 
 cd build/es5-bundled/
~~~

comprueba la ruta a tu carpeta public 
-
~~~ 
  ls  ../../../firebaseDeployShop/public
~~~

copia el contenido 
-
~~~ 
cp -r ./* ../../../firebaseDeployShop/public
~~~

* cp significa copiar
* -r significa recursivo
* ./* significa todos los contenidos del direcctorio actual
* ../ significa vete un directorio arriba 

ve al directorio public y verifica que copiaste bien
-
~~~ 
cd ../../../firebaseDeployShop/public
 ls
404.html  bower_components  data    index.html     push-manifest.json  src
app.yaml  bower.json        images  manifest.json  service-worker.js
~~~



desplegar tu app
-
~~~ 
cd ..
firebase deploy 
~~~

despues de unos segundos veras algo como 
-
~~~ 
=== Deploying to 'shop-e7df4'...

i  deploying hosting
i  hosting: preparing public directory for upload...
✔  hosting: 210 files uploaded successfully
i  starting release process (may take several minutes)...

✔  Deploy complete!

Project Console: https://console.firebase.google.com/project/shop-e7df4/overview
Hosting URL: https://shop-e7df4.firebaseapp.com

~~~

si abres en un navegador la url de hosting  podras ver tu proyecto, pero ahora subido a un servidor disponible para todos en interne 
-
~~~ 
https://shop-e7df4.firebaseapp.com
~~~
